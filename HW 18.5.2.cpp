#include <iostream>
#include <stack>
#include <string>

using namespace std;




int main()
{
	stack<string> myStack;

	myStack.push("5"); 
	myStack.push("3");
	myStack.push("2");

	cout << "Number of strings on the stack " << myStack.size() << endl;

	myStack.pop();

	while (!myStack.empty())
	{
		cout << "popping " << myStack.top() << endl;
		myStack.pop();
	}

	myStack.push("765");

	cout << myStack.top() << endl;

	while (!myStack.empty())
	{
		cout << "popping " << myStack.top() << endl;
		myStack.pop();
	}

	cout << "Number of strings on the stack " << myStack.size() << endl;
}
